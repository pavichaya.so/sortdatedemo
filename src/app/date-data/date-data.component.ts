import { Component, OnInit } from '@angular/core';
import { DateService, Date } from '../date.service';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-date-data',
  templateUrl: './date-data.component.html',
  styleUrls: ['./date-data.component.css'],
})
export class DateDataComponent implements OnInit {
  title = 'datedata';

  pipe = new DatePipe('en-US');
  dates: Date[] = [];

  constructor(private dateService: DateService) {}

  ngOnInit(): void {
    this.dateService.getDate().then((dates) => {
      let test :Date[] = [];
      dates.forEach( (value) => {
        const day = value.date.substr(0, 2);
        const month = value.date.substr(3, 2);
        const year = value.date.substr(6, 4);
        const date = new Date(year, month, day);
        let test1 = { title: value.title, date: date};
        test.push(test1)
        console.log(test1)
      });
      this.dates = test;
      // console.log(this.dates);
    });
    // console.log(this.dateService.getDate())
  }
}
