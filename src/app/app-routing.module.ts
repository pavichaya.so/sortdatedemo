import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DateDataComponent } from './date-data/date-data.component';

const routes: Routes = [
  { path: 'date', component: DateDataComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
