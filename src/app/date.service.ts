import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Date {
  title: any;
  date: any;
}

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor(private http: HttpClient) {}

  getDate(){
    return this.http.get<any>('assets/date.json')
      .toPromise()
      .then(res => <Date[]>res.data)
      .then(data => {return data;});
  }
}
